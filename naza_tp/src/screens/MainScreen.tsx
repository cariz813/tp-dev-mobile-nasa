// React
import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  StyleSheet,
  ScrollView
} from "react-native";
// Components
import PicturesList from "../components/PicturesList";
import ApodSelected from "../components/ApodSelected";
// Styles
import GlobalStyles from "../style/GlobalStyles";
// Types
import { _NazaApod, _NazaApodList } from "../types/NazaApod";
// Services
import {getNasaPictures} from '../services/GetApod'



/*
* Perso function for add 'n' days to date
*/
const addDaysToDate = (date: Date, days: number): Date => {
  date.setDate(date.getDate() + days);
  return date;
};


/*
* MainScreen component
*/
const MainScreen = () => {
  // main hook for store all results (apods) from Nasa API
  const [nasaPictures, setNasaPictures] = useState<_NazaApodList>([]);
  // hook for sttore selected Apod -> if null no apod selected
  const [apodSelected, setApodSelected] = useState<_NazaApod | null>(null);
  // hook for loaders -> true or false
  const [apodLoading, setApodLoading] = useState<boolean>(true);


  useEffect(() => {
    const getNasaList = async () => {
      setNasaPictures([]); // clean nasaPictures
      setApodSelected(null) // clean ApodSelected
      setApodLoading(true) // Display loader

      let pictures: _NazaApodList = [];
      let tmp: _NazaApod | null = null

      // loop for get 10 last apods -> date.now - 1 day * iteration
      for (let i = 0; i < 10; i++) {
        tmp = await getNasaPictures(addDaysToDate(new Date(), -1 * i)) // my service for request nasa api
        if (tmp) pictures.push(tmp) // if good response -> add to tmp pictures list
      }
      setNasaPictures(pictures); // set pictures list in NasaPictures hook for reload page
      setApodLoading(false) // hide loader
      if (pictures[0]) setApodSelected(pictures[0]) // set default Apod selected in ApodSelected hook
    }
    getNasaList();
    return () => {};
  }, []);

  const setSelected = (selected: _NazaApod) => { setApodSelected(selected) }; // perso function for set selected apod for send it in props


  return (
    <SafeAreaView style={GlobalStyles.droidSafeArea}>
      <StatusBar style="dark" />
      <ScrollView style={{ backgroundColor: '#000000'}}>
        <PicturesList setSelected={setSelected} nasaPictures={nasaPictures} loading={apodLoading} />
        {apodSelected ? <ApodSelected apod={apodSelected} /> : <></>}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title: {
    fontWeight: "800",
    fontSize: 54,
    padding: 16,
  },
  selected: {
    color: "white",
  },
  container: {
    flex: 1,
    backgroundColor: "#000000",
  },
  loader : {
    marginTop: 100,
    justifyContent: 'center',
  }
});

export default MainScreen;
