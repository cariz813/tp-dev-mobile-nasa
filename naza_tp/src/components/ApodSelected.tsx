// React
import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import axios from "axios";

// Expo
import { useFonts } from "expo-font";
import { AppLoading } from "expo";

//Types
import { _NazaApod, _NazaApodList } from "../types/NazaApod";

type Props = {
  apod: _NazaApod;
};

/*
* ApodSelected components
* @apod
*/
const ApodSelected: React.FC<Props> = ({ apod }) => {

  // Load sf_compact_heavy && sf_compact_regular fonts
  let [fontsLoaded] = useFonts({
    sf_compact_heavy: require("../../assets/fonts/sf_compact_heavy.otf"),
    sf_compact_regular: require("../../assets/fonts/sf_compact_regular.ttf"),
  });

  // UseEffect for control apod passed in parameters
  useEffect(() => {
    console.log("SELECTED: ", apod);
    return () => {};
  }, []);

  if (!fontsLoaded) return <AppLoading />;
  else
    return (
      <>
        <ImageBackground
          source={{
            uri: apod.url,
          }}
          style={styles.backgroundImage}
        >
          <Text style={styles.title}>{apod ? apod.title : ""}</Text>
          <Text style={styles.explanation}>{apod ? apod.explanation : ""}</Text>
        </ImageBackground>
      </>
    );
};

const styles = StyleSheet.create({
  container: {
    height: 90,
    paddingLeft: 26,
    marginBottom: 59,
    backgroundColor: "transparent",
  },
  text: {
    color: "white",
  },
  title: {
    color: "white",
    fontSize: 32,
    fontFamily: "sf_compact_heavy",
    fontWeight: "bold",
    paddingRight: 100,
    marginBottom: 20,
    marginTop: 300, // plus jolie que 356
    backgroundColor: "transparent",
  },
  explanation: {
    color: "white",
    fontSize: 17,
    fontFamily: "sf_compact_regular",
    paddingRight: 25,
    marginBottom: 50,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover",
    paddingTop: 30,
  },
});

export default ApodSelected;
